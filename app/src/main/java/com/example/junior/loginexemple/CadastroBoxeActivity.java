package com.example.junior.loginexemple;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.junior.loginexemple.model.Banca;
import com.example.junior.loginexemple.model.Usuario;

public class CadastroBoxeActivity extends AppCompatActivity implements View.OnClickListener{
    private EditText descricao,cnpj,nomeProprietario,senha;
    private Button cadastrar,cancelar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro_boxe);

        descricao=(EditText) findViewById(R.id.descricao_boxe);
        cnpj=(EditText) findViewById(R.id.cnpj_boxe);
        nomeProprietario=(EditText) findViewById(R.id.nome_proprietario_boxe);
        senha=(EditText) findViewById(R.id.senha_boxe);

        cadastrar=(Button) findViewById(R.id.btn_cadastrar);
        cadastrar.setOnClickListener(this);

        cancelar=(Button) findViewById(R.id.btn_cancelar);
        cancelar.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_cadastrar:
                Banca banca=new Banca(descricao.getText().toString(),nomeProprietario.getText().toString(),cnpj.getText().toString());
                banca.save();
                Usuario user=new Usuario(descricao.getText().toString(),senha.getText().toString(),banca.getCliente(),banca.getId());
                user.save();
                Toast.makeText(CadastroBoxeActivity.this, "Boxe cadastrado com sucesso", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(this,MainActivity.class));
                break;

            case R.id.btn_cancelar:
                Toast.makeText(CadastroBoxeActivity.this,"Cadastro cancelado", Toast.LENGTH_SHORT).show();

                startActivity(new Intent(this,MainActivity.class));

                break;
        }
    }
}
