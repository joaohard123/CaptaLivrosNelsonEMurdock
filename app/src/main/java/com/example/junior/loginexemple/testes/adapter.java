package com.example.junior.loginexemple.testes;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.junior.loginexemple.R;
import com.example.junior.loginexemple.model.Usuario;

import java.util.List;

/**
 * Created by junior on 12/05/2016.
 */
public class adapter extends BaseAdapter {
    private List<Usuario> itens;
    private LayoutInflater inflater;

    public adapter(Activity activity,List<Usuario> usuario){
        this.itens=usuario;
        this.inflater=LayoutInflater.from(activity);
    }
    @Override
    public int getCount() {
        return itens.size();
    }

    @Override
    public Object getItem(int position) {
        return itens.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v=convertView;
        v=inflater.inflate(R.layout.layout_lista_usuario,null);
        TextView id=(TextView) v.findViewById(R.id.id_usuario);
        TextView nome=(TextView) v.findViewById(R.id.nome_usuario);
        TextView senha=(TextView) v.findViewById(R.id.senha_usuario);
        TextView tipo=(TextView) v.findViewById(R.id.tipo_usuario);
        TextView usuario_id=(TextView) v.findViewById(R.id.usuario_id);

        id.setText(itens.get(position).getId().toString());
        nome.setText(itens.get(position).getUsuario().toString());
        senha.setText(itens.get(position).getSenha().toString());

        if(itens.get(position).getTipoUsuario().equals(true)){
            tipo.setText("Cliente");
        }else if(itens.get(position).getTipoUsuario().equals(false)){
            tipo.setText("Vendedor");

        }
        String usuarioId=String.valueOf(itens.get(position).getUsuario_id());

        usuario_id.setText(usuarioId);

        return v;
    }
}
