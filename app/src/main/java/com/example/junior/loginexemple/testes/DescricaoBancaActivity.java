package com.example.junior.loginexemple.testes;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.junior.loginexemple.MainActivity;
import com.example.junior.loginexemple.R;
import com.example.junior.loginexemple.dao.UsuariosCadastrados;
import com.example.junior.loginexemple.model.Banca;

public class DescricaoBancaActivity extends AppCompatActivity {
    private TextView descricao,nome,cnpj;
    private Button sair;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_descricao_banca);

        descricao=(TextView) findViewById(R.id.descricao);
        nome=(TextView) findViewById(R.id.nome);
        cnpj=(TextView) findViewById(R.id.cnpj);
        sair=(Button) findViewById(R.id.sair);
        sair.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(DescricaoBancaActivity.this, ListaUsuariosActivity.class));
            }
        });

        UsuariosCadastrados dao=new UsuariosCadastrados();
       // String idPassado=String.valueOf(dao.getId());
        //descricao.setText(idPassado);
        //Intent intent=getIntent();
        //Bundle bundle=intent.getExtras();

       Banca banca=Banca.findById(Banca.class,dao.getId());

        descricao.setText(banca.getDescricao());
        nome.setText(banca.getNomeProprietario());
        cnpj.setText(banca.getCnpj());

    }
}
