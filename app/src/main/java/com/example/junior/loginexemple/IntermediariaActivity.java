package com.example.junior.loginexemple;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class IntermediariaActivity extends AppCompatActivity implements View.OnClickListener{
    private Button usuario,banca;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intermediaria);

        usuario = (Button)findViewById(R.id.btn_usuario);
        banca = (Button)findViewById(R.id.btn_banca);

        usuario.setOnClickListener(this);
        banca.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_usuario:

                startActivity(new Intent(IntermediariaActivity.this,CadastrarUsuarioActivity.class));
                finish();

                break;
            case R.id.btn_banca:
                startActivity(new Intent(IntermediariaActivity.this,CadastroBoxeActivity.class));
                finish();

                break;
        }
    }
}
