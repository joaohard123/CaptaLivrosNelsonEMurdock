package com.example.junior.loginexemple;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.junior.loginexemple.dao.UsuarioLogado;
import com.example.junior.loginexemple.model.Cliente;
import com.example.junior.loginexemple.model.Livro;
import com.example.junior.loginexemple.model.Usuario;

public class CadastrarLivroActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText titulo,autor,ano,serie,preco;
    private RadioGroup estadoConserv;
    private Button btnSalvar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_cadastrar_livro);
        titulo=(EditText) findViewById(R.id.tituloLivro);
        autor=(EditText) findViewById(R.id.autorLivro);
        ano=(EditText) findViewById(R.id.anoLivro);
        serie=(EditText) findViewById(R.id.serieLivro);
        estadoConserv=(RadioGroup) findViewById(R.id.estadoLivro);
        preco=(EditText) findViewById(R.id.precoLivro);
        btnSalvar=(Button) findViewById(R.id.btn_cadastrar);
        btnSalvar.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_cadastrar:
                UsuarioLogado dao=new UsuarioLogado();
                Usuario user=Usuario.findById(Usuario.class,dao.getId());
                Livro livro=new Livro(titulo.getText().toString(),autor.getText().toString(),ano.getText().toString(),serie.getText().toString(),"Bom",preco.getText().toString(),user);
                livro.save();

                Toast.makeText(CadastrarLivroActivity.this, "Salvo com sucesso", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(this,ListaLivrosActivity.class));
                break;
        }
    }
}
