package com.example.junior.loginexemple.model;

import com.google.common.primitives.Booleans;
import com.orm.SugarRecord;

/**
 * Created by junior on 12/05/2016.
 */
public class Banca extends SugarRecord {
    private String descricao;
    private String nomeProprietario;
    private String cnpj;

    public Banca(){

    }

    public Banca(String descricao,String nomeProprietario,String cnpj){
        this.setDescricao(descricao);
        this.setNomeProprietario(nomeProprietario);
        this.setCnpj(cnpj);
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getNomeProprietario() {
        return nomeProprietario;
    }

    public void setNomeProprietario(String nomeProprietario) {
        this.nomeProprietario = nomeProprietario;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public Boolean getCliente(){
        return false;
    }
}
