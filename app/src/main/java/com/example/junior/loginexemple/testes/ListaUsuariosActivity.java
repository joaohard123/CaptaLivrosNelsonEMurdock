package com.example.junior.loginexemple.testes;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.junior.loginexemple.MainActivity;
import com.example.junior.loginexemple.R;
import com.example.junior.loginexemple.dao.UsuariosCadastrados;
import com.example.junior.loginexemple.model.Usuario;

import org.w3c.dom.Text;

import java.util.List;

public class ListaUsuariosActivity extends AppCompatActivity implements AdapterView.OnItemClickListener{

    private Button sair;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_usuarios);

        sair=(Button) findViewById(R.id.sair);
        sair.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(ListaUsuariosActivity.this, "Ate logo", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(ListaUsuariosActivity.this, MainActivity.class));
            }
        });

        List usuarios= Usuario.listAll(Usuario.class);
        ListView lv=(ListView) findViewById(R.id.lista_usuarios);
        adapter adp=new adapter(this,usuarios);
        lv.setAdapter(adp);
        lv.setOnItemClickListener(this);


    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        TextView tipo=(TextView) view.findViewById(R.id.tipo_usuario);
        TextView idPassado=(TextView) view.findViewById(R.id.usuario_id);
        if(tipo.getText().equals("Vendedor")) {

            Intent intent=new Intent(this,DescricaoBancaActivity.class);
            UsuariosCadastrados dao=new UsuariosCadastrados();
            Long idP=Long.parseLong(idPassado.getText().toString());
            dao.setId(idP);

            Toast.makeText(ListaUsuariosActivity.this, "Descricao do Vendedor", Toast.LENGTH_SHORT).show();
            startActivity(intent);
        }

    }
}
