package com.example.junior.loginexemple.model;

import com.orm.SugarRecord;

import java.util.List;

/**
 * Created by junior on 12/05/2016.
 */
public class Usuario extends SugarRecord{
    private String usuario;
    private String senha;
    private Boolean tipoUsuario;
    private long usuario_id;

    public Usuario(){

    }

    public Usuario(String usuario,String senha,Boolean tipoUsuario,long usuario_id){
        this.setUsuario(usuario);
        this.setSenha(senha);
        this.setTipoUsuario(tipoUsuario);
        this.setUsuario_id(usuario_id);
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public Boolean getTipoUsuario() {
        return tipoUsuario;
    }

    public void setTipoUsuario(Boolean tipoUsuario) {
        this.tipoUsuario = tipoUsuario;
    }

    public long getUsuario_id() {
        return usuario_id;
    }

    public void setUsuario_id(long usuario_id) {
        this.usuario_id = usuario_id;
    }

    /*public List<Livro> getLivros(){
        return Livro.find(Livro.class,"usuario=?",getId().toString());
    }*/
}
