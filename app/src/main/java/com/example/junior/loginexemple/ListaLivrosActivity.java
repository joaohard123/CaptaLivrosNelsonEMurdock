package com.example.junior.loginexemple;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.example.junior.loginexemple.dao.UsuarioLogado;
import com.example.junior.loginexemple.model.Livro;
import com.example.junior.loginexemple.model.Usuario;

import java.util.List;

public class ListaLivrosActivity extends AppCompatActivity {

    private UsuarioLogado dao=new UsuarioLogado();
    private Usuario user;
    private List<Livro> livros;
    private TextView novo,sair;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_livros);

        novo=(TextView) findViewById(R.id.novo);
        sair=(TextView) findViewById(R.id.sair);
        user = Usuario.findById(Usuario.class, dao.getId());

        String idP=String.valueOf(dao.getId());



        if(dao.getTipo()==true){

            livros=Livro.listAll(Livro.class);
            novo.setEnabled(false);
            novo.setText("Perfil");


        }else if(dao.getTipo()==false) {

            livros=Livro.find(Livro.class,"usuario=?",idP);

            novo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(ListaLivrosActivity.this,CadastrarLivroActivity.class));
                }
            });

        }

        ListView lv=(ListView) findViewById(R.id.listaLivros);
        Adapter adapter=new Adapter(this,livros);
        lv.setAdapter(adapter);



        sair.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dao.setId(0);
                startActivity(new Intent(ListaLivrosActivity.this,MainActivity.class));
            }
        });
    }
}
