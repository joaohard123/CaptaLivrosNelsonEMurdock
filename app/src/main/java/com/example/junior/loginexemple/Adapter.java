package com.example.junior.loginexemple;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.junior.loginexemple.model.Livro;

import java.util.List;

/**
 * Created by junior on 12/05/2016.
 */
public class Adapter extends BaseAdapter {
    private List<Livro> livros;
    private LayoutInflater inflater;

    public Adapter(Activity activity, List<Livro> livros){
        this.livros=livros;
        this.inflater=LayoutInflater.from(activity);
    }
    @Override
    public int getCount() {
        return livros.size();
    }

    @Override
    public Object getItem(int position) {
        return livros.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v=convertView;

        v=inflater.inflate(R.layout.layout_lista_livros,null);
        ImageView foto=(ImageView) v.findViewById(R.id.foto_livro);
        TextView titulo=(TextView) v.findViewById(R.id.titulo_livro);
        TextView autor=(TextView) v.findViewById(R.id.titulo_livro);
        //TextView usuario=(TextView) v.findViewById(R.id.usuario_livro);
        TextView idLivro=(TextView) v.findViewById(R.id.id_livro);

        foto.setImageResource(R.drawable.livros);
        idLivro.setText(livros.get(position).getId().toString());
        titulo.setText(livros.get(position).getTitulo());
        autor.setText(livros.get(position).getAutor());


        return v;
    }
}
