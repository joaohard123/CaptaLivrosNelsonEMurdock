package com.example.junior.loginexemple.model;

import com.orm.SugarRecord;

/**
 * Created by junior on 12/05/2016.
 */
public class Livro extends SugarRecord {
    private String titulo;
    private String autor;
    private String ano;
    private String serie;
    private String estadoLivro;
    private String preco;
    private Usuario usuario;

    public Livro(){

    }

    public Livro(String titulo,String autor,String ano,String serie,String estadoLivro,String preco,Usuario usuario){
        this.setTitulo(titulo);
        this.setAutor(autor);
        this.setAno(ano);
        this.setSerie(serie);
        this.setEstadoLivro(estadoLivro);
        this.setPreco(preco);
        this.setUsuario(usuario);

    }
    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getAno() {
        return ano;
    }

    public void setAno(String ano) {
        this.ano = ano;
    }

    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public String getEstadoLivro() {
        return estadoLivro;
    }

    public void setEstadoLivro(String estadoLivro) {
        this.estadoLivro = estadoLivro;
    }

    public String getPreco() {
        return preco;
    }

    public void setPreco(String preco) {
        this.preco = preco;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
}


