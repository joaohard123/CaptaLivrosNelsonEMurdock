package com.example.junior.loginexemple.dao;

import com.example.junior.loginexemple.model.Usuario;

/**
 * Created by junior on 12/05/2016.
 */
public class UsuarioLogado {
    private static long id=1;
    private static boolean tipo=true;


    public static long getId() {
        return id;
    }

    public static void setId(long id) {
        UsuarioLogado.id = id;
    }

    public static void setTipo(boolean tipo){
        UsuarioLogado.tipo=tipo;
    }

    public static boolean getTipo(){
        return tipo;
    }
}
