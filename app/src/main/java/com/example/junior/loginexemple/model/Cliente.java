package com.example.junior.loginexemple.model;

import com.orm.SugarRecord;

import java.util.List;

/**
 * Created by junior on 12/05/2016.
 */
public class Cliente extends SugarRecord {
    private String nome;
    private String email;

    public Cliente(){}

    public Cliente(String nome,String email) {
        this.setNome(nome);
        this.setEmail(email);
    }


    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getCliente() {
        return true;
    }
}


