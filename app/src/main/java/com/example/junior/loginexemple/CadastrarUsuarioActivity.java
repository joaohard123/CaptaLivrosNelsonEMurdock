package com.example.junior.loginexemple;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.junior.loginexemple.model.Cliente;
import com.example.junior.loginexemple.model.Usuario;

public class CadastrarUsuarioActivity extends AppCompatActivity implements View.OnClickListener{
    private EditText nome, senha, email;
    private Button salvar, cancelar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastrar_usuario);

        nome = (EditText)findViewById(R.id.nome_usuario);
        senha = (EditText)findViewById(R.id.senha_usuario);
        email = (EditText)findViewById(R.id.email_usuario);

        salvar= (Button)findViewById(R.id.btn_cadastrar);
        cancelar=(Button) findViewById(R.id.btn_cancelar);

        salvar.setOnClickListener(this);
        cancelar.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_cadastrar:
                Cliente cliente = new Cliente(nome.getText().toString(),email.getText().toString());
                cliente.save();
                Usuario user=new Usuario(email.getText().toString(),senha.getText().toString(), cliente.getCliente(),cliente.getId());
                user.save();
                Toast.makeText(CadastrarUsuarioActivity.this,"Usuario "+ nome.getText().toString() + " salvo com sucesso", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(this,MainActivity.class));
                break;

            case R.id.btn_cancelar:
                Toast.makeText(CadastrarUsuarioActivity.this,"Cadastro cancelado" + " salvo com sucesso", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(this,MainActivity.class));

                break;
        }
    }
}
