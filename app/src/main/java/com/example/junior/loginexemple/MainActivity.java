package com.example.junior.loginexemple;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.junior.loginexemple.dao.UsuarioLogado;
import com.example.junior.loginexemple.model.Usuario;
import com.example.junior.loginexemple.testes.ListaUsuariosActivity;

import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    private EditText usuario, senha;
    private Button logar;
    private TextView link,link_cadastrados;
    private UsuarioLogado dao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        usuario = (EditText)findViewById(R.id.edt_email);
        senha = (EditText)findViewById(R.id.edt_senha);
        logar = (Button)findViewById(R.id.btn_logar);
        link = (TextView)findViewById(R.id.txt_login);
        link_cadastrados = (TextView)findViewById(R.id.lista_usuario_cadastrados);


        logar.setOnClickListener(this);
        link.setOnClickListener(this);
        link_cadastrados.setOnClickListener(this);

        dao=new UsuarioLogado();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_logar:
                List<Usuario> usuarios=Usuario.findWithQuery(Usuario.class,"Select * from Usuario where usuario=? and senha=?",usuario.getText().toString(),senha.getText().toString());

                if(!usuarios.isEmpty()) {
                    Toast.makeText(MainActivity.this, "Usuario logado "+ usuarios.get(0).getUsuario(), Toast.LENGTH_SHORT).show();
                    dao.setId(usuarios.get(0).getId());
                    dao.setTipo(usuarios.get(0).getTipoUsuario());

                    startActivity(new Intent(this, ListaLivrosActivity.class));

                }else{
                    Toast.makeText(MainActivity.this, "Por favor efetuar cadastro", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.txt_login:
                startActivity(new Intent(this,IntermediariaActivity.class));
                break;
            case R.id.lista_usuario_cadastrados:
                startActivity(new Intent(this,ListaUsuariosActivity.class));

                break;
        }
    }
}
